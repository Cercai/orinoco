
/**
 * return the vue of a list-item execpt when one parameter is absent or not compliant
 * @param {*} name : the name of the teddy
 * @param {*} price : the price of the teddy
 * @param {*} description : the description of the teddy
 * @param {*} imageUrl : the imageUrl of the teddy
 * @param {*} id : the id of the teddy
 */
 function cart_teddy_item(name, price, description, imageUrl, id, number, color) {

    if (!id || !name || !price || !description || !imageUrl) {
        return "NaT"; // Not a Teddy
    }

    if (!name.match(name_regex) || !description.match(description_regex)
        || !imageUrl.match(imageUrl_regex) || !id.match(id_regex)) {
        return "NaT";
    }

    if (isNaN(price)) {
        return "NaT";
    }

    return `<div class="row bg-white m-sm-2 my-3 p-2 border">
        <div class="col-sm-4 col-md-5 col-xl-4">
            <a href="teddy.html?id=${id}"><img src=${imageUrl} alt="a cute teddy" width="250"></a>
        </div>
        <div class="col-sm-6 col-md-5 col-xl-6">
            <h3>${name} (${color})</h3>
            <p class="container">
                ${int_to_double(price)} €
            </p>
            <p class="container">
                ${description}
            </p>
        </div>
        <div class="col-sm-2 col-md-2 col-xl-2 d-flex flex-column justify-content-center">
            <p>quantité: ${number}</p>
            <a href="cart.html?delete=${id}-${color}" class="btn btn-danger"><i class="far fa-trash-alt"></i></a>
        </div>
    </div>`;
}
