

function recipe_row(name, price, quantity, color, rowColor) {

    if (!name || !price || !quantity || !color) {
        return "";
    }

    if (!name.match(name_regex) || !color.match(name_regex)
        || typeof price != 'number' || typeof quantity != 'number') {
        return "";
    }

    if (isNaN(price) || isNaN(quantity)) {
        return "";
    }

    return `<tr class="row ${rowColor}">
                <td class="col-4">
                    ${name} (${color})
                </td>
                <td class="col-4">
                    ${quantity}
                </td>
                <td class="col-4">
                    ${int_to_double(price)} €
                </td>
            </tr>`;
}

function recipe_total(_total, rowColor) {

    if (!_total || isNaN(_total)) {
        return '';
    }

    return `<tr class="row ${rowColor}">
                <td class="col-4">
                    
                </td>
                <td class="col-4">
                    total
                </td>
                <td class="col-4">
                    <b>${int_to_double(_total)} €</b>
                </td>
            </tr>
        </table>
    <legend>Articles commandés</legend>
</fieldset>`;
}


 function print_recipe(recipe) {
    let lastName, firstName, email, city, address, orderId, total, recipeInnerHtml;
    total = 0;
    recipeInnerHtml = '';

    try {
        lastName = recipe.contact.lastName;
        firstName = recipe.contact.firstName;
        email = recipe.contact.email;
        city = recipe.contact.city;
        address = recipe.contact.address;
        orderId = recipe.orderId;
    } catch (e) {
        console.error(`In function print_recipe() - ${e}`);
        return;
    }

    recipeInnerHtml = `<h1>Récapitulatif de la commande</h1>
        <fieldset class="row w-100">
            <ul class="col-6">
                <li style="list-style-type:none">
                    lastName: ${lastName}
                </li>
                <li style="list-style-type:none">
                    firstName: ${firstName}
                </li>
                <li style="list-style-type:none">
                    email: ${email}
                </li>
            </ul>
            <ul class="col-6">
                <li style="list-style-type:none">
                    city: ${city}
                </li>
                <li style="list-style-type:none">
                    address: ${address}
                </li>
            </ul>
            <legend>Vos coordonnées</legend>
        </fieldset>
        <fieldset class="w-100">
            <p>n° de commande : ${orderId}</p>
            <table class="table table-hover"><tr class="row">
            <th scope="col" class="col-4">
                name
            </th>
            <th scope="col" class="col-4">
                quantité
            </th>
            <th scope="col" class="col-4">
                price
            </th>
        </tr>`;

    let ids = get_ids();
    let teddiesPrinted = [];
    let lastId = 1;
    let rowColor = 'table-light';
                
    ids.forEach(id => {
        
        fetch(urlApiTeddy + id)
        .then(teddyJSON=>{
            teddyJSON.json().then(teddy => {
                
                let colors = get_colors_with_id(id);
                
                colors.forEach(color => {

                    if (!teddiesPrinted.includes(id + color)) {
                        let number = get_number_with_id_and_color(id, color);
                        recipeInnerHtml += recipe_row(teddy.name, teddy.price, number, color, rowColor);
                        rowColor = rowColor === 'table-light' ? 'table-secondary' : 'table-light';
                        teddiesPrinted.push(id + color);
                        total += number * teddy.price;
                    }
                });

                if (lastId === ids.length) {
                    recipeHtml.innerHTML = recipeInnerHtml+= recipe_total(total);
                }
                lastId++;
            });
        });
    });
}
