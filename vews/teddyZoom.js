
/**
 * return the vue of a list-item execpt when one parameter is absent or not compliant
 * @param {*} name : the name of the teddy
 * @param {*} price : the price of the teddy
 * @param {*} description : the description of the teddy
 * @param {*} imageUrl : the imageUrl of the teddy
 * @param {*} id : the id of the teddy
 */
function teddyZoom(name, price, description, imageUrl, id) {
    if (!id || !name || !price || !description || !imageUrl) {
        return "NaT"; // Not a Teddy
    }
    if (!name.match(name_regex) || !description.match(description_regex)
        || !imageUrl.match(imageUrl_regex) || !id.match(id_regex)) {
        return "NaT";
    }
    
    if (isNaN(price)) {
        return "NaT";
    }

    return `<div class="col-sm-5">
                <img src="${imageUrl}" alt="" width="300">
            </div>
            <div class="col-sm-7">
                <h2>${name}</h2>
                <p>${int_to_double(price)} €</p>
                <label for="select_colors">How would like your teddy ?</label>
                <select name="colors" id="select_colors"></select>
                
                <p>${description}</p>
            </div>`;
}