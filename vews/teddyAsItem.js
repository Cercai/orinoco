
/**
 * return the vue of a list-item execpt when one parameter is absent or not compliant
 * @param {*} name : the name of the teddy
 * @param {*} price : the price of the teddy
 * @param {*} description : the description of the teddy
 * @param {*} imageUrl : the imageUrl of the teddy
 * @param {*} id : the id of the teddy
 */
function teddyAsItem(name, price, description, imageUrl, id) {

    if (!id || !name || !price || !description || !imageUrl) {
        return "NaT"; // Not a Teddy
    }

    if (!name.match(name_regex) || !description.match(description_regex)
        || !imageUrl.match(imageUrl_regex) || !id.match(id_regex)) {
        return "NaT";
    }

    if (isNaN(price)) {
        return "NaT";
    }

    return `<div class="row bg-white m-sm-2 my-3 p-2 border">
        <div class="col-sm-4 col-md-5 col-xl-4">
            <a href="teddy.html?id=${id}"><img src=${imageUrl} alt="a cute teddy" width="250"></a>
        </div>
        <div class="col-sm-8 col-md-7 col-xl-8">
            <h3>${name}</h3>
            <p class="container">
                ${int_to_double(price)} €
            </p>
            <p class="container">
                ${description}
            </p>
        </div>
    </div>`;
}


