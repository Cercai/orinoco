
function get_ids() {
    let _teddies = JSON.parse(localStorage.getItem('teddies'));
    let _IDs = [];

    if (!_teddies) {
        return ;
    }

    _teddies.forEach(teddy => {
        if (!_IDs.includes(teddy.teddyId)) {
            _IDs.push(teddy.teddyId);
        }
    })

    return _IDs;
}


function get_colors_with_id(id) {
    let colors = [];

    let teddies = JSON.parse(localStorage.getItem('teddies'));
    teddies.forEach(teddy => {
        if (teddy.teddyId === id) {
            colors.push(teddy.color);
        }
    });
    return colors;
}


function save_recipe(recipe) {
    localStorage.setItem('recipe', JSON.stringify(recipe));
}


function fetch_recipe() {
    return JSON.parse(localStorage.getItem('recipe'));
}


function get_number_with_id_and_color(id, color) {
    let number = 0;

    let teddies = JSON.parse(localStorage.getItem('teddies'));
    teddies.forEach(teddy => {
        if (teddy.teddyId === id && teddy.color === color) {
            number++;
        }
    });
    return number;
}


function delete_teddy(id, color) {
    
    if (color.includes('%20')) {
        color = color.replace('%20', ' ');
    }

    let teddies = JSON.parse(localStorage.getItem('teddies'));
    
    for (let i = 0; i < teddies.length; i++) {
        if (teddies[i].teddyId === id && teddies[i].color === color) {
            teddies.splice(i, 1);
            localStorage.setItem('teddies', JSON.stringify(teddies));
            return;
        }
    }
}
