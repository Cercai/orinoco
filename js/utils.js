
function int_to_double(int) {
    if (typeof int === 'number') {
        return (int/100).toFixed(2);
    }
    return int;
}
