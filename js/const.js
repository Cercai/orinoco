/*** regex ***/
const name_regex = /^[A-Za-z ]*$/;
const description_regex = /^[A-Za-z \.,]*$/;
const imageUrl_regex = /^http:\/\/localhost:[0-9]{4}\/images\/teddy_[0-9]\.(jpg|bmp|jpeg)$/;
const id_regex = /^[a-zA-Z0-9]{24}$/;
const firstname_lastname_regex = /^[a-z ,.'-]+$/i;
const city_regex = /^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/;
const address_regex = /^[0-9a-z ,.'-]+$/i;
const email_regex = /^[A-Za-z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;



/*** url ***/
const urlApiTeddies = "http://localhost:3000/api/teddies";
const urlApiTeddy = "http://localhost:3000/api/teddies/";

/*** test's constants ***/
const correctTeddyId = "5beaabe91c9d440000a57d96";
