
document.getElementById('submit_btn').addEventListener('click', (event) => {
    event.stopPropagation();
    event.preventDefault();

    let lastname = document.getElementById('input_lastname').value;
    let firstname = document.getElementById('input_firstname').value;
    let city = document.getElementById('input_city').value;
    let address = document.getElementById('input_address').value;
    let email = document.getElementById('input_email').value;

    if (!lastname.match(firstname_lastname_regex)) {
        alert('lastname not correct');
        return;
    }
    if (!firstname.match(firstname_lastname_regex)) {
        alert('firstname not correct');
        return;
    }
    if (!city.match(city_regex)) {
        alert('city not correct');
        return;
    }
    if (!address.match(address_regex)) {
        alert('address not correct');
        return;
    }
    if (!email.match(email_regex)) {
        alert('email not correct');
        return;
    }
    
    let contact = {
        firstName: firstname,
        lastName: lastname,
        address: address,
        city: city,
        email: email
    };

    let products = get_ids();

    let order = {
        contact,
        products
    };

    let myInit = {  method: 'POST',
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8'
                    },
                    body: JSON.stringify(order)
                };

    fetch('http://localhost:3000/api/teddies/order', myInit)
    .then(function(response) {
        response.json()
        .then(recipe => {
            save_recipe(recipe);
            window.location = "recipe.html";
        }

        );
    })
    .catch(function(err) {
        alert(err);
    });
});
