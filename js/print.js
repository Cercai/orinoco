
function printTeddies(htmlList) {

    if (!htmlList) {
        console.error(`In function printTeddies(), htmlList n'existe pas !`);
        return;
    }

    fetch(urlApiTeddies)
    .then(teddies=>{
        teddies.json()
        .then(teddies=>{

            if (teddies.constructor !== [].constructor)
                    return;

            teddies.forEach(teddy => {
                
                if (!(teddy instanceof Object))
                    return;
                
                let listItem = document.createElement("li");
                listItem.innerHTML = teddyAsItem(teddy.name, teddy.price, teddy.description, teddy.imageUrl, teddy._id);                    
                htmlList.appendChild(listItem);
            });
        })
        .catch(err => {
            console.error(`In function printTeddies(), impossible to json.parse the input: ${err}`);
        });
    })
    .catch(err => {
        console.error(`In function printTeddies(), unable to connect to the server: ${err}`);
    });
}


function printTeddy(teddyHtml, id) {

    fetch(urlApiTeddy + id)
    .then(teddies=>{
        teddies.json()
        .then(teddy=>{
            if (!(teddy instanceof Object)) {
                throw new Error("teddy is not an object");
            }

            if (!teddy.hasOwnProperty('colors')) {
                throw new Error("there is no attribute named 'colors'");
            }
            
            let div = document.createElement("div");
            div.innerHTML = teddyZoom(teddy.name, teddy.price, teddy.description, teddy.imageUrl, teddy._id);
            div.classList.add("row");
            teddyHtml.appendChild(div);
            let select_colors = document.getElementById('select_colors');
            
            teddy.colors.forEach(color => {
                let option = document.createElement("option");
                option.innerHTML = color;

                select_colors.appendChild(option);
            });
        })
        .catch(err => {
            console.error(`In function printTeddy(), impossible to json.parse the input: ${err}`);
        });
    })
    .catch(err => {
        console.error(`In function printTeddy(), unable to connect to the server: ${err}`);
    });
}


function printCart(cartHtmlElement, priceHtmlElement, ids) {
    let listTeddies = "";
    let teddiesPrinted = [];
    let price = 0;
    priceHtmlElement.innerHTML = '0 €';

    ids.forEach(id => {

        fetch(urlApiTeddy + id)
        .then(teddyJSON=>{

            teddyJSON.json().then(teddy => {
                let colors = get_colors_with_id(id);
                console.log(teddy);
                colors.forEach(color => {
                    console.log(color);
                    if (!teddiesPrinted.includes(id + color)) {
                        let number = get_number_with_id_and_color(id, color);

                        console.log(teddy);
                        listTeddies += cart_teddy_item(teddy.name, teddy.price, teddy.description, teddy.imageUrl, teddy._id, number, color);

                        cartHtmlElement.innerHTML = listTeddies;
                        teddiesPrinted.push(id + color);
                        price += number * teddy.price;

                        priceHtmlElement.innerHTML = int_to_double(price) + ' €';
                    }
                });
            });
        });
    });
}
