
function it(message, expr) {
    if (!expr) {
        // false, null, undefined, 0, "", NaN
        throw new Error(message);
    }

    return true;
}

/*
function assertEqual(testNo, str1, str2) {
    if (str1 != str2)
        throw new Error("test " + testNo + " not passed !");
}

function assertNotEqual(testNo, str1, str2) {
    if (str1 == str2)
        throw new Error("test " + testNo + " not passed !");
}*/

function testCase(_name, _tests) {
    let testOk = 0;

    for (let test in _tests) {
        if (!/^test/.test(test)) {
            continue;
        }

        try {
            _tests[test]();
            testOk++;
        } catch (e) {
            console.log(test + " failed: " + e.message);
        }
    }

    console.log(testOk + " tests ok");
}

