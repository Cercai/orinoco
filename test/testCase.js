
let test1 = ["teddy rinner", "1 million", "un ours assez costaud", "http://localhost:3000/images/teddy_5.jpg"]
let test2 = ["balou", "1500", "un ours assez costaud", "http://localhost:3000/images/teddy_5.jpeg"]
let test3 = ["teddy", 500, "un ours assez costaud", "http://localhost:3000/images/teddy_01.jpg"]
let test4 = ["teddy rinner", "900", "un ours vraiment costaud !", "http://localhost:3000/images/teddy_1.jpg"]
let test5 = ["petit ours brun", "800", "un petit ours", "http://localhost:3000/images/teddy_1.jpg"]

let test6 = ["teddy rinner", 900, "un ours vraiment costaud !", "http://localhost:3000/images/teddy_5.jpg"]
let test7 = ["myTeddy", "800", "un petit ours", "http://localhost:3000/images/teddy_1.jpg"]

let test8 = ["teddy", 4, 5, "red", "class-light"]
let test9 = ["marcel dzeq", 1000, 2, "jaune"]
let test10 = ["teddy", "5000", 10, "red", "class-light"]
let test11 = ["charles-henry", 10, 10, "red", "class-light"]

testCase("list-item-Test", {
    "test 1": function() {
        it(" shall not be a teddy (incorrect price)", 
        teddyAsItem(test1[0], test1[1], test1[2], test1[3], correctTeddyId) === "NaT")
    },
    "test 2": function() {
        it(" is a teddy", 
        teddyAsItem(test2[0], test2[1], test2[2], test2[3], correctTeddyId) !== "NaT")
    },
    "test 3": function() {
        it(" shall not be a teddy (incorrect imageUrl)", 
        teddyAsItem(test3[0], test3[1], test3[2], test3[3], correctTeddyId) === "NaT")
    },
    "test 4": function() {
        it(" shall not be a teddy (incorrect description)", 
        teddyAsItem(test4[0], test4[1], test4[2], test4[3], correctTeddyId) === "NaT")
    },
    "test 5": function() {
        it(" shall not be a teddy (incorrect id)", 
        teddyAsItem(test2[0], test2[1], test2[2], test2[3], "41g68rdg5rd") === "NaT")
    },



    "test 6": function() {
        it(" shall not be a teddy (incorrect description)", 
        teddyZoom(test6[0], test6[1], test6[2], test6[3], correctTeddyId) === "NaT")
    },
    "test 7": function() {
        it(" is a teddy", 
        teddyZoom(test7[0], test7[1], test7[2], test7[3], correctTeddyId) !== "NaT")
    },



    //recipe_row(name, price, quantity, color, rowColor)
    "test 8": function() {
        it(" should not return an empty string", 
        recipe_row(test8[0], test8[1], test8[2], test8[3], test8[4]) !== "")
    },
    "test 9": function() {
        it(" should not return an empty string", 
        recipe_row(test9[0], test9[1], test9[2], test9[3], test9[4]) !== "")
    },
    "test 10": function() {
        it(" should return an empty string (incorrect description)", 
        recipe_row(test10[0], test10[1], test10[2], test10[3], test10[4]) === "")
    },
    "test 11": function() {
        it(" should return an empty string (incorrect description)", 
        recipe_row(test11[0], test11[1], test11[2], test11[3], test11[4]) === "")
    },

});